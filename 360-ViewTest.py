import cv2
from imutils.video import VideoStream
from time import sleep

def main():

    front = VideoStream(src=1).start()
    left = VideoStream(src=2).start()
    right = VideoStream(src=3).start()
    rear = VideoStream(src=0).start()


    while True:

        imFront = front.read()
        imLeft = left.read()
        imRight = right.read()
        imRear = rear.read()

        cv2.imshow("Front", imFront)
        cv2.imshow("Left", imLeft)
        cv2.imshow("Right", imRight)
        cv2.imshow("Rear", imRear)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    front.stop()
    left.stop()
    right.stop()
    rear.stop()

    cv2.destroyAllWindows()

main()