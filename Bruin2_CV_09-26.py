import numpy as np
import cv2
from matplotlib import pyplot as plt
import math

vidcap = cv2.VideoCapture(0)
laneLeft = []
laneRight = []
leftLane = []
rightLane = []

def main():
    while True:
        image = cv2.imread('lanes.jpg')
        #ret, image = vidcap.read()
        #image = cv2.flip(image, -1)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (3,3), 0)
        #edges = cv2.Canny(blur, 10, 11, 3) # Edge Detection
        #gEdges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)
        #setupSobel = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
        #sobel = cv2.Sobel(setupSobel, cv2.CV_64F,1,0,5) # Data type is CV_64
        #blurSobel = cv2.GaussianBlur(sobel, (9,9),0)
        #absSobel = np.absolute(blurSobel)
        #sobel8 = np.uint8(absSobel) # Data type is CV_8u
        #graySobel8 = cv2.cvtColor(sobel8, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(blur, 100, 300) # get edges from the Sobel filter
        #gEdges = cv2.cvtColor(edges, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(edges, 127, 255, 0)
        ctr, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        minLineLength = 40
        maxLineGap = 25
        lines = cv2.HoughLinesP(edges, 6, np.pi/180, 100, minLineLength, maxLineGap)

        contour = 0
        while (contour < len(contours)):
            cv2.putText(image, str(contour), (contours[contour][0][0][0],contours[contour][0][0][1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1, cv2.LINE_AA)
            contour += 1

        if lines is not None:
            processLines(image, lines)

        #print('New Array')
        #for edge in laneLeft:
            #for x1, y1, x2, y2 in edge:
                #print(edge)
                          

        #cv2.imshow('Sobel', graySobel8)
        #cv2.putText(image, 'Kyle Weberg', (5, 50), cv2.FONT_HERSHEY_DUPLEX, 2, (150, 0, 150), 2)
        cv2.drawContours(image, contours, -1, (0,255,0), 3)
        cv2.imshow('Edges', edges)
        cv2.imshow('Blur', blur)
        cv2.imshow('Lined Image', image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vidcap.release()
    cv2.destroyAllWindows()

def processLines(image, lines):

    for line in lines:
        for x1, y1, x2, y2 in line:
            slope = (y2 - y1) / (x2 - x1)
            cv2.line(image,(x1,y1),(x2,y2),(147,105,255),2) # Put lines into original image

            # If slope is -, put into laneLeft array
            # if slope is +, put into laneRight array
            # Backwards because the (0,0) is at the upper left hand corner, with the +y direction being downward
            if slope < 0:
                #np.append(laneLeft,line)
                laneLeft.append(line)
            elif slope > 0:
                #np.append(laneRight, line)
                laneRight.append(line)

            #print(slope)
            cv2.putText(image, str(format(slope,'0.4f')), (x1,y1), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 1, cv2.LINE_AA)

        
    smallX = 10000  # Arbitrarily large number, hopefully exceeding any other x-coordinate. Meant for finding the minimum value
    smallY = 10000
    largeX = 0
    largeY = 0

    for line in laneLeft:
        for x1, y1, x2, y2 in line:
             
            #Line endpoints: (smallest X, largest Y), (largest X, smallest Y)
            #                (X1, Y1)                 (X2, Y2)
            if x1 < smallX:
                smallX = x1
            if y1 < smallY:
                smallY = y1
            if x2 > largeX:
                largeX = x2
            if y2 > largeY:
                largeY = y2
    leftLane = [smallX, largeY, largeX, smallY]
    #leftLaneLL = '(' + str(laneLeft[0]) + ',' + str(laneLeft[1]) + ')'
    #leftLaneTL = '(' + str(laneLeft[2]) + ',' + str(laneLeft[3]) + ')'

    #cv2.putText(image, leftLaneLL, (laneLeft[0], laneLeft[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
    #cv2.putText(image, leftLaneTL, (laneLeft[2], laneLeft[3]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)

    smallX = 10000
    smallY = 10000
    largeX = 0
    largeY = 0
        
    for line in laneRight:
        for x1, y1, x2, y2 in line:

            #Line endpoints: (smallest X, largest Y), (largest X, smallest Y)
            #                (X1, Y1)                 (X2, Y2)
            if x1 < smallX:
                smallX = x1
            if y1 < smallY:
                smallY = y1
            if x2 > largeX:
                largeX = x2
            if y2 > largeY:
                largeY = y2

    rightLane = [smallX, smallY, largeX, largeY]

    # Put final line markings on image
    #cv2.line(image, (leftLane[0], leftLane[1]), (leftLane[2], leftLane[3]),(0,0,255),5) # Left lane line
    #cv2.line(image, (rightLane[0], rightLane[1]), (rightLane[2], rightLane[3]), (255,0,0), 5)


main()