import cv2
import numpy
import matplotlib.image as mpimg

def main():
    while True:
        # 'Perspective' image taken roughly 5' 4" off the ground using the Microsoft Cinema LifeCam
        image = cv2.imread('perspective.jpg')
        #image = mpimg.imread('perspective.jpg')

        cv2.imshow('Original', image)

        M = cv2.getPerspectiveTransform(numpy.float32([[380,450], [630,450], [650,610],[330,610]]), numpy.float32([[10,10], [380,10], [380, 380], [10, 380]]))
        print(M)
        transformed = cv2.warpPerspective(image, M, (400,400))

        cv2.imshow('Transformed', transformed)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

main()