import numpy as np
import cv2
from matplotlib import pyplot as plt
import math

vidcap = cv2.VideoCapture(0)

def main():
    while True:
        image = cv2.imread('lanes.jpg')
        #ret, image = vidcap.read()
        #image = cv2.flip(image, -1)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (3,3), 0)
        edges = cv2.Canny(blur, 100, 300) # get edges from the Sobel filter
        ret, thresh = cv2.threshold(edges, 127, 255, 0)
        ctr, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Process the contours
        getLanes(image, contours)

        # Draw the images
        cv2.drawContours(image, contours, -1, (0,255,0), 3)
        cv2.imshow('Edges', edges)
        cv2.imshow('Blur', blur)
        cv2.imshow('Contours', image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vidcap.release()
    cv2.destroyAllWindows()



def getLanes(image, contours):
    poly = []

    for contour in contours:
        #Calculate the perimeter of the contour
        #peri = cv2.arcLength(contour, False)
        #cv2.putText(image, str(peri), (contour[len(contour)/2][0][0]+10, contour[len(contour)/2][0][1]+10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1, cv2.LINE_AA)
        
        # Extract x and y values for every point in the contour 
        xValues = []
        yValues = []
        for point in contour:
            xValues.append(point[0][0])
            yValues.append(point[0][1])
        
        pointSlope = []
        slopeSum = 0
        slope = 0
        for i in range(0, len(min(xValues, yValues))):
            pointSlope.append(yValues[i] / xValues[i])
        for slope in pointSlope:
            slopeSum += slope
        
        slope = slopeSum / len(pointSlope)

        if slope > 0.5 and slope < 1.5:
            # Fit a second degree polynomial to the contour
            # We will get a 1x3 array containing A, B, and C for the polynomial
            # y=Ax^2 + Bx + C
            poly.append(np.polyfit(xValues, yValues, 2))
            #cv2.putText(image, str(slope), (xValues[int(len(xValues) / 2)], yValues[int(len(yValues) / 2)]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
        
        
        #radius = (1 + (2*poly[0] * yValues[int((len(yValues) - 1)) / 2] + poly[1])^2) / abs(2*poly[0])
        # Rcurve = (1 + (Ay+B^2))^(3/2)/abs(2A)
        
        ''' Test output
        print('xValues')
        print(xValues)
        print('contour')
        print(len(contour))
        print(len(contour) % 2)
        if len(contour) % 2 == 1:
            cv2.putText(image, poly[-1], (contour[int((len(contour) / 2) + 1)][0][0], contour[int((len(contour) / 2) + 1)][0][1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
        else:
            cv2.putText(image, poly[-1], (contour[int(len(contour) / 2)][0][0], contour[int(len(contour) / 2)][0][1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
        print('y={%0.2f}x^2 + {%0.2f}x + {%0.2f}', poly[0], poly[1], poly[2])
        '''

main()